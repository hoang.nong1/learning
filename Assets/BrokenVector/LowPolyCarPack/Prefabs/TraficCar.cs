﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraficCar : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] public float scaleChange = 30;
    [SerializeField] public float movementSpeed = 0.1f;
    [SerializeField] public float rotationWayX = 1;
    [SerializeField] public string idType;

    public TraficCar(float scaleChange, float movementSpeed, float rotationWayX)
    {
        this.scaleChange = scaleChange;
        this.movementSpeed = movementSpeed;
        this.rotationWayX = rotationWayX;
    }

    void Start()
    {
        ChangeScale();
        RotationCar();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        autoMoveTraficCar();
    }

    private void ChangeScale()
    {
        transform.localScale = new Vector3(scaleChange, scaleChange, scaleChange);
    }

    private void autoMoveTraficCar()
    {
        transform.Translate(new Vector3(movementSpeed,0,0));
    }

    public void UpdateInfo(float rotation, Vector3 startPosition)
    {
        rotationWayX = rotation;
        transform.localPosition = startPosition;
    }

    public void ResetPosition(Vector3 startPosition)
    {
        transform.localPosition = startPosition;
    }
    
    private void RotationCar()
    {
        if(rotationWayX == 1)
            transform.Rotate(0, 180 ,0);
    }
}
