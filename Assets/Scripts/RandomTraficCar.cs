﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomTraficCar : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private int TraficCarLenght;
    [SerializeField] private int RotationWay;
    [SerializeField] private float DistanceCar;
    public string TraficCarType;
    private List<TraficCar> ListCar;

    public RandomTraficCar(int traficCarLenght, int rotationWay, float distanceCar, string traficCarType, TraficCar[] listCar)
    {
        TraficCarLenght = traficCarLenght;
        RotationWay = rotationWay;
        DistanceCar = distanceCar;
        TraficCarType = traficCarType;
        foreach (TraficCar car in listCar)
        {
            if (ListCar != null) 
                ListCar.Add(car);
        }
    }

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    

    public void SettingTraficCar()
    {
        int nextCar = 0;
        Vector3 startPosition = new Vector3(15 * RotationWay,0,0);
        foreach (TraficCar traficCar in ListCar)
        {
            traficCar.UpdateInfo(RotationWay, startPosition);
        }
    }
    
}
