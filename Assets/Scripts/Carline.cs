﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Carline : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private float moveSpeed;
    [SerializeField] private MeshRenderer meshLine;
    [SerializeField] private float autoMoveSpeed;
    public Camera mainCamera;
    public bool isOutOfCamera;
    void Start()
    {
        meshLine = GetComponent<MeshRenderer>();
        isOutOfCamera = false;
    }

    // Update is called once per frame
    void Update()
    {
        //transform.Translate(Vector3.back);
    }

    public void changeColorLine(Color color)
    {
        meshLine.material.color = color;
    }

    private void autoMove()
    {
        
    }
    
}
