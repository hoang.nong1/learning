﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraficCarManage : MonoBehaviour
{
    // Start is called before the first frame update

    public List<TraficCar> ListTraficCar;
    public TextAsset TypeCarJson;
    [Serializable]
    public class TraficCarType
    {
        public string Type;
        public float Speed;
        public int Distance;
    }

    [Serializable]
    public class TraficCarTypeList
    {
        public TraficCarType[] CarType;
    }
    public TraficCarTypeList traficCarTypeList = new TraficCarTypeList();
    
    void Start()
    {
        LoadJsonTypeCar();
        SettingTypeTraficCar();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void LoadJsonTypeCar()
    {
        traficCarTypeList = JsonUtility.FromJson<TraficCarTypeList>(TypeCarJson.text);
    }

    private void SetTypeTraficCar(float Speed, string Type)
    {
        TraficCar[] traficCars = Resources.LoadAll<TraficCar>(Type);
        foreach (TraficCar car in traficCars)
        {
            car.idType = Type;
            car.movementSpeed = Speed;
        }
        AddTraficCarTypeToList(traficCars);
    }

    private void SettingTypeTraficCar()
    {
        foreach (TraficCarType type in traficCarTypeList.CarType)
        {
            SetTypeTraficCar(type.Speed, type.Type);
        }
    }

    private void AddTraficCarTypeToList(TraficCar[] traficCars)
    {
        foreach (TraficCar trafic in traficCars)
        {
            ListTraficCar.Add(trafic);
        }
    }
    
}
