﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using Random = System.Random;

public class RandomLine : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] public List<Carline> lineList;
    [SerializeField] private int lineListLenght;
    [SerializeField] private Carline _carline;
    [SerializeField] private List<Color> listColorLine;
    [SerializeField] private int indexDelete;
    [SerializeField] private int MaxLenghtPlace;
    public TraficCarManage TraficCarManage;
    private int _linePlace = 0;
    private int _lineColor = 0;
    private int sameColor = -1;
    private Random rd = new Random();
    [SerializeField] private Transform ListLines;
    [SerializeField] public List<RandomTraficCar> traficCarList;
    private void Awake()
    {
        //ListLines = transform.GetComponentInChildren<Transform>();
        for (int i = 0; i < lineListLenght; i++)
        {
            CreateLineList(_carline, i);
        }
        RandomCarline();
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        SortListCar();
    }

    private void CreateLineList(Carline nextCarline, int i)
    {
        Carline Carline = Instantiate<Carline>(nextCarline, nextCarline.transform.position + new Vector3(0, 0, 1 * i),
            Quaternion.identity);
        Carline.transform.parent = ListLines.transform;
        Carline.name = "Line-" + i;
        lineList.Add(Carline);
    }

    private void RandomCarline()
    {
        int sameline;
        int countPlace = 0;
        for (int i = 0; i < lineListLenght; i += countPlace)
        {
            countPlace = rd.Next(1, MaxLenghtPlace - 2);
            sameline = rd.Next(0, listColorLine.Count);
            
            if (sameColor == sameline)
                while (sameColor != sameline)
                {
                    sameline = rd.Next(0, listColorLine.Count); 
                }
            sameColor = sameline;
            LinePlaceColor(i,countPlace,GetColor(sameline));
            if(i + countPlace >= lineListLenght)
                break;
        }
    }

    private Color RandomColor()
    {
        return listColorLine[rd.Next(0, listColorLine.Count)];
    }

    private Color GetColor(int color)
    {
        return listColorLine[color];
    }

    private void LinePlaceColor(int begin, int end, Color color)
    {
        for (int j = begin ; j < begin + end; j++)
        {
            if(j < lineListLenght)
                lineList[j].changeColorLine(color);
        }
    }

    private void SortListCar()
    {
        if (lineList[indexDelete].isOutOfCamera)
        {
            if (_linePlace == 0)
            {
                _linePlace = rd.Next(1, MaxLenghtPlace);
                int color = rd.Next(0, listColorLine.Count);
                if (sameColor == color)
                    while (sameColor != color)
                    {
                        color = rd.Next(0, listColorLine.Count); 
                    }
                sameColor = color;
                _lineColor = color;
            }
               
            lineList[indexDelete].transform.position =
                lineList[lineListLenght - 1].transform.position + new Vector3(0, 0, 1);
            
            lineList[indexDelete].changeColorLine(GetColor(_lineColor));
            lineList.Add(lineList[indexDelete]);
            lineList.Remove(lineList[indexDelete]);
            
            _linePlace--;
        }
    }
    
    
}
