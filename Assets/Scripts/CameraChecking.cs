﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class CameraChecking : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Camera mainCamera;
    [SerializeField] public RandomLine RandomLine;
    private CinemachineVirtualCamera _virtualCamera;
    void Start()
    {
        mainCamera = gameObject.GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckCar();
    }

    private void CheckCar()
    {
        var lineOutOfViewPosition = RandomLine.lineList[0].transform.position;
        var viewPos = mainCamera.WorldToViewportPoint(lineOutOfViewPosition);
        
        if (viewPos.x < -1 || viewPos.x > 1 || viewPos.y < -1 || viewPos.y > 1 || viewPos.z <= 0)
        {
            Debug.Log("Vi tri: " + viewPos);
            Debug.Log("Vi tri Obj: " + lineOutOfViewPosition);
            RandomLine.lineList[0].isOutOfCamera = true;
        }
        else
            RandomLine.lineList[0].isOutOfCamera = false;
        
    }
    
}
