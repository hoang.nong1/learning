﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Player : MonoBehaviour
{
    [SerializeField] private float distanceJump;
    [SerializeField] private float forceJumps;
    private Rigidbody rb;
    private bool _isGround = true;
    private Vector3 _jumpHeight;
    private Vector3 rootPosition;
    private Vector3 _gravityPosition;
    private Transform _transform;
    [SerializeField] private float timeJumps;
    private float rootTime;
    private int isMoving = 0;
    private TouchControl _touchControl = new TouchControl();
    
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
        _gravityPosition = transform.localPosition;
        _transform = transform;
        _touchControl.DistanceSwipe = 10;
    }

    // Update is called once per frame
    void Update()
    {
        MovementPlayer();
    }

    private void MovementPlayer()
    {
        if ((CheckMovement() != 0) && _isGround)
        {
            var position = _transform.localPosition;
            rootPosition = position;
            _jumpHeight = position + OnMovementPlayer();
            _gravityPosition = position + OnGravity();
            rb.useGravity = false;
            _isGround = false;
        }
        if (Math.Round(Vector3.Distance(_transform.localPosition, _jumpHeight),1) - 0.1 <= 0 && !_isGround)
        {
            _transform.localPosition = Vector3.Lerp(_transform.localPosition, _gravityPosition, forceJumps * Time.deltaTime);
            _jumpHeight = _transform.localPosition;
            
        }
        if (!_isGround)
            _transform.localPosition = Vector3.Lerp(_transform.localPosition, _jumpHeight, forceJumps * Time.deltaTime);
        else
        {
            rb.useGravity = true;
            _transform.localPosition = _gravityPosition;
        }
        
    }

    public void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Wall"))
            Destroy(gameObject);
        if (other.gameObject.CompareTag("Player"))
            rb.isKinematic = true;
        if (other.gameObject.CompareTag("Ground"))
        {
            _isGround = true;
            
        }
        rb.useGravity = true;
    }
    

    public void OnCollisionExit(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
            rb.isKinematic = false;
    }

    private Vector3 OnMovementPlayer()
    {
        if (CheckMovement() == 1)
            return new Vector3( timeJumps, timeJumps, 0);
        else if (CheckMovement() == -1)
            return new Vector3( - timeJumps, timeJumps, 0);
        else if (CheckMovement() == 2)
            return new Vector3(0, timeJumps, timeJumps);
        else
        {
            return Vector3.zero;
        }
    }

    private Vector3 OnGravity()
    {
        if (CheckMovement() == 1)
            return new Vector3( distanceJump, 0, 0);
        else if (CheckMovement() == -1)
            return new Vector3( - distanceJump, 0, 0);
        else if (CheckMovement() == 2)
            return new Vector3(0, 0, distanceJump);
        else
        {
            return Vector3.zero;
        }
    }

    private int CheckMovement()
    {
        int result = Int32.MaxValue;
        
#if UNITY_ANDROID
        if (_isGround)
        {
            result = SwipeTouchControl();
           
        }
#endif
        
#if UNITY_EDITOR
        if (_isGround)
        {
            if (Input.GetKeyDown(KeyCode.D))
                result = 1;
            else if (Input.GetKeyDown(KeyCode.A))
                result = -1;
            else if (Input.GetKeyDown(KeyCode.Space))
                result = 2;
            else
            {
                result = 0;
            }
        }
#endif
        return result;
    }

    private int SwipeTouchControl()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                _touchControl.fp = touch.position;
                _touchControl.lp = touch.position;
            }
            else if (touch.phase == TouchPhase.Moved)
            {
                 _touchControl.lp = touch.position;
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                if (Math.Abs(_touchControl.lp.x - _touchControl.fp.x) > _touchControl.DistanceSwipe ||
                    Math.Abs(_touchControl.lp.y - _touchControl.fp.y) > _touchControl.DistanceSwipe)
                {
                    if (Math.Abs(_touchControl.lp.x - _touchControl.fp.x) >
                        Math.Abs(_touchControl.lp.y - _touchControl.fp.y))
                    {
                        if (_touchControl.lp.x > _touchControl.fp.x)
                        {
                            Debug.LogError("Right");
                            return 1;
                        }
                        else
                        {
                            Debug.LogError("Left");
                            return -1;
                        }
                            
                    }
                }
                else
                    return 2;
            }
        }

        return 0;
    }
    public class TouchControl
    {
        public Vector3 fp;
        public Vector3 lp;
        public float DistanceSwipe;
    }

}
